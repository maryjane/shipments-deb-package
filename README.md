## Disclaimer: 

**This is an unofficial DEB package of shipments.**

I made this package because: 
- I wanted to practice packaging applications as DEB packages

This was tested only on my device. And it worked. Being a non official package, you are on your own when you install this. Don’t annoy, your distro’s developers with bugs about this package. Bug me.

## Downloading the package

You can download it from here: https://git.disroot.org/maryjane/shipments-deb-package/raw/branch/master/shipments_0.1-1_all.deb

## installing

From the same directoty where the package is run the command:

`sudo apt install ./shipments_0.1-1_all.deb`


## Building the package from the deb source

This package was built based on the repo:

`hhttps://git.disroot.org/maryjane/shipments` from the `debian/latest` branch. So you can check the commit log to see every change I did to it.

The repo itself is a copy of: which is a copy of: `https://git.sr.ht/~martijnbraam/shipments`

You can build the package yourself on a Debian based system by installing: 

`sudo apt install git build-essential devscripts`

`apt install --no-install-recommends git-buildpackage` 

Check the Build-Depends dependencies here: 

https://git.disroot.org/maryjane/shipments/src/branch/debian/latest/debian/control

And running the commands:

`git clone https://git.disroot.org/maryjane/shipments.git`

cd into the `shipments` directory

`git checkout debian/latest`

and run the command: 

`debuild -us -uc -b` 

